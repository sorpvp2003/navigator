class RegisterDataModel {
  final String reportUser, reportEmail, reportPassword;

  RegisterDataModel(
      this.reportUser,
      this.reportEmail,
      this.reportPassword);
}
