import 'package:flutter/material.dart';
import 'pages/my_home_page.dart';

void main() {
  runApp(const MyFirstNavigationApp());
}

class MyFirstNavigationApp extends StatelessWidget {
  const MyFirstNavigationApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Navigator',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              textStyle: const TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              backgroundColor: Colors.black,
              foregroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(7),
              ),
              padding: const EdgeInsets.all(16),
              elevation: 5,
            ),
          )
      ),
      home: const MyHomePage(title: 'First Navigation'),
    );
  }
}
